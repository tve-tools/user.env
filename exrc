" ----  General settings        ----
set ts=2
set sw=2
set expandtab
set ic
"
" ----  Key mapping             ----
map J Jj
map v :s :$:-->:j
map q :s :^:#:j
map Q :s :^#::j

map t  :1,$s :\s*$::<CR>:1,1s :^:":<CR>:2,$s :^:	+ " :<CR>:1,$s :$: ":<CR>
map T :1,1s :^\s*"\s*::<CR>:2,$s :^\s*+\s*"\s*::<CR>:1,$s :"\s*$::<CR>

map v :%s :.*:\L&:
map V :%s :.*:\U&:
abb cmt # let's try that
