#!/usr/bin/env bash

REPO_URL=$1
NEW_REPO_NAME=$2

[ "x$REPO_URL" == "x" ] && echo "provide git repo url as first arg..." && exit 0
[ "x$NEW_REPO_NAME" == "x" ] && echo "provide new repo name as second arg..." && exit 0

set -x 
git clone $REPO_URL $NEW_REPO_NAME
#REPO=$(basename $REPO_URL)
#REPO="${REPO%.*}"
REPO=$NEW_REPO_NAME
cd $REPO
git remote add gl https://gitlab.com/tve-pro/sg/repos/$REPO.git
git push gl master
set -x

# ###
# copy multiple branches

#git clone git@sgithub.fr.world.socgen:BigDataHub-I2T/role_app_elasticsearch_utilities.git
#cd $REPO
#git branch -r
#git switch role_mig_68_717_iso
#git remote add gl https://gitlab.com/tve-pro/sg/repos/$REPO.git
#git push --all gl


#git clone git@sgithub.fr.world.socgen:dlhybrid/namenode-mirror.git 
#cd $(basename dlhybrid/namenode-mirror) 
#git remote add gl https://gitlab.com/tve-pro/sg/repos/dlhybrid/namenode-mirror.git 
#git push gl
#
#git clone --depth 1 git@sgithub.fr.world.socgen:dlhybrid/namenode-mirror.git "$(basename dlhybrid/namenode-mirror)-shallow" 
#cd $(basename dlhybrid/namenode-mirror) 
#rm -rf .git 
#git init --initial-branch=master 
#git remote add gl https://gitlab.com/tve-pro/sg/repos/dlhybrid/namenode-mirror_shallow.git 
#git add . 
#git commit -m init -a 
#git push gl

